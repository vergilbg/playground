using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using CircuitBreakerPolicy.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Polly;
using Polly.Extensions.Http;

namespace CircuitBreakerPolicy
{
    public class Startup
    {
        private static readonly Uri PlaceholderUri = new Uri("https://PLACEHOLDER-NOT-A-REAL-ADDRESS/");

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddHttpClient("TestClient", client => client.BaseAddress = new Uri("https://mock.codes/"));

            services
                .AddHttpClient("PrimaryClient", client => client.BaseAddress = PlaceholderUri)
                .ConfigureHttpMessageHandlerBuilder(builder =>
                {
                    // in reality gets settings from secrets manager
                    var settings = new Settings
                    {
                        // assume primary host is down
                        Host = "https://httpstat.us/500"
                    };

                    builder.PrimaryHandler = new HttpClientHandler
                    {
                        Credentials = new NetworkCredential(settings.Username, settings.Password),
                        AutomaticDecompression = DecompressionMethods.GZip
                    };

                    var primaryBaseAddress = new Uri(settings.Host);

                    builder.AdditionalHandlers.Add(new ReplaceRequestUriHandler(primaryBaseAddress));
                });

            services
                .AddHttpClient("FailoverClient", client => client.BaseAddress = PlaceholderUri)
                .ConfigureHttpMessageHandlerBuilder(builder =>
                {
                    // in reality gets settings from secrets manager
                    var settings = new Settings
                    {
                        Host = "https://httpstat.us/200"
                    };
                    
                    builder.PrimaryHandler = new HttpClientHandler
                    {
                        Credentials = new NetworkCredential(settings.Username, settings.Password),
                        AutomaticDecompression = DecompressionMethods.GZip
                    };

                    var failoverAddress = new Uri(settings.Host);

                    builder.AdditionalHandlers.Add(new ReplaceRequestUriHandler(failoverAddress));
                });

            services.AddSingleton(GetFallbackPolicy());
            services.AddSingleton(GetCircuitBreakerPolicy());
            services.AddTransient<IWeatherService, WeatherService>();
            services.AddTransient<IWeatherService2, WeatherService2>();
            services.AddControllers();
        }

        static IAsyncPolicy<HttpResponseMessage> GetCircuitBreakerPolicy()
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .CircuitBreakerAsync(
                    1,
                    TimeSpan.FromSeconds(15),
                    (r, t) =>
                    {
                        Debug.WriteLine("Circuit broken");
                    },
                    () => { Debug.WriteLine("Circuit half open"); },
                    () => { Debug.WriteLine("Circuit closed."); })
                .WithPolicyKey("CircuitBreaker");
        }
        
        static IAsyncPolicy<HttpResponseMessage> GetFallbackPolicy()
        {
            return Policy<HttpResponseMessage>
                .Handle<Exception>()
                .FallbackAsync(
                    fallbackAction: async ct =>
                    {
                        var weatherForecast = await Task.FromResult<HttpResponseMessage>(new HttpResponseMessage());

                        return weatherForecast;
                    },
                    onFallbackAsync: async e =>
                    {
                        Debug.WriteLine(e.Exception != null
                            ? $"Exception: {e.Exception.Message}"
                            : $"Status code: {e.Result.StatusCode}");
                    }
                )
                .WithPolicyKey("Fallback"); ;
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }

    public class ReplaceRequestUriHandler : DelegatingHandler
    {
        private readonly Uri _newUri;

        public ReplaceRequestUriHandler(Uri newUri)
        {
            _newUri = newUri;
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage requestMessage, CancellationToken cancellationToken)
        {
            var relativeAddress = new Uri("https://PLACEHOLDER-NOT-A-REAL-ADDRESS/").MakeRelativeUri(requestMessage.RequestUri);
            requestMessage.RequestUri = new Uri(_newUri, relativeAddress);

            return base.SendAsync(requestMessage, cancellationToken);
        }
    }
}
