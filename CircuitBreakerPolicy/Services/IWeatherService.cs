﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CircuitBreakerPolicy.Services
{
    public interface IWeatherService
    {
        Task<string> Get();
    }
}