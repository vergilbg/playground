﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CircuitBreakerPolicy.Services
{
    public interface IWeatherService2
    {
        Task<string> Get();
    }
}