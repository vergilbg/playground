﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Polly;
using Polly.Wrap;

namespace CircuitBreakerPolicy.Services
{
    public class WeatherService2 : IWeatherService2
    {
        private readonly HttpClient _primaryClient;
        private readonly HttpClient _failoverClient;
        private readonly AsyncPolicyWrap<HttpResponseMessage> _policy;
        private readonly IAsyncPolicy<HttpResponseMessage> _circuitBreaker;

        public WeatherService2(IHttpClientFactory clientFactory, IEnumerable<IAsyncPolicy<HttpResponseMessage>> policies)
        {
            _primaryClient = clientFactory.CreateClient("PrimaryClient");
            _failoverClient = clientFactory.CreateClient("FailoverClient");
            _circuitBreaker = policies.First(p => p.PolicyKey == "CircuitBreaker");

            _policy = Policy<HttpResponseMessage>
                .Handle<Exception>()
                .FallbackAsync(_ => CallFallbackForecastApi())
                .WrapAsync(_circuitBreaker);
        }

        public async Task<string> Get()
        {
            var response = await _policy
                .ExecuteAsync(() => CallForecastApi());

            if (response.IsSuccessStatusCode) 
                return response.StatusCode.ToString();

            response = await CallFallbackForecastApi();
            return response.StatusCode.ToString();

        }

        public async Task<HttpResponseMessage> CallForecastApi()
        {
            Debug.WriteLine("Calls: https://httpstat.us/500");
            return await _primaryClient.GetAsync("/");
        }

        public async Task<HttpResponseMessage> CallFallbackForecastApi()
        {
            Debug.WriteLine("Calls: https://httpstat.us/200");
            return await _failoverClient.GetAsync("/");
        }
    }
}
