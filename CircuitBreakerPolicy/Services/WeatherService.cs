﻿using System.Net.Http;
using System.Threading.Tasks;

namespace CircuitBreakerPolicy.Services
{
    public class WeatherService : IWeatherService
    {
        private readonly HttpClient _client;

        public WeatherService(IHttpClientFactory clientFactory)
        {
            _client = clientFactory.CreateClient("TestClient");
        }

        public async Task<string> Get()
        {
            var response = await _client.GetAsync("/500");

            return response.StatusCode.ToString();
        }
    }
}
