﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using CircuitBreakerPolicy.Services;

namespace CircuitBreakerPolicy.Controllers
{
    [ApiController]
    // [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IWeatherService _weatherService;
        private readonly IWeatherService2 _weatherService2;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IWeatherService weatherService, IWeatherService2 weatherService2)
        {
            _logger = logger;
            _weatherService = weatherService;
            _weatherService2 = weatherService2;
        }

        [HttpGet]
        [Route("[controller]/1")]
        public async Task<string> Get()
        {
            return await _weatherService.Get();
        }

        [HttpGet]
        [Route("[controller]/2")]
        public async Task<string> Get2()
        {
            return await _weatherService2.Get();
        }
    }
}
