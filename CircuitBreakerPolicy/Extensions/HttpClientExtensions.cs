﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace CircuitBreakerPolicy.Extensions
{
    public static class HttpClientExtensions
    {
        public static Task<HttpResponseMessage> PostAsync(this HttpClient httpClient, string requestUri, HttpContent content, HttpCompletionOption completionOption)
            => PostAsync(httpClient, CreateUri(requestUri), content, completionOption, CancellationToken.None);

        public static Task<HttpResponseMessage> PostAsync(this HttpClient httpClient, string requestUri, HttpContent content, HttpCompletionOption completionOption, CancellationToken cancellationToken)
            => PostAsync(httpClient, CreateUri(requestUri), content, completionOption, cancellationToken);

        public static Task<HttpResponseMessage> PostAsync(this HttpClient httpClient, Uri requestUri, HttpContent content, HttpCompletionOption completionOption)
            => PostAsync(httpClient, requestUri, content, completionOption, CancellationToken.None);

        public static Task<HttpResponseMessage> PostAsync(this HttpClient httpClient, Uri requestUri, HttpContent content, HttpCompletionOption completionOption, CancellationToken cancellationToken)
        {
            var httpRequest = new HttpRequestMessage(HttpMethod.Post, requestUri)
            {
                Content = content
            };

            return httpClient.SendAsync(httpRequest, completionOption, cancellationToken);
        }

        private static Uri CreateUri(string uri) =>
            string.IsNullOrEmpty(uri) ? null : new Uri(uri, UriKind.RelativeOrAbsolute);
    }
}