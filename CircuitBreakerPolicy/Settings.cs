namespace CircuitBreakerPolicy
{
    public class Settings
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
    }
}